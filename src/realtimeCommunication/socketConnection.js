import io from "socket.io-client";
import {
  setPendingFriendsInvitations,
  setFriends,
  setOnlineUsers,
} from "../store/actions/friendsActions";
import { setGroups } from "../store/actions/group";
import {
  groupChatReceived,
  newMessageReceived,
} from "../store/actions/groupChatAction";
import store from "../store/store";
import { updateDirectChatHistoryIfActive } from "../shared/utils/chat";

let socket = null;

export const connectWithSocketServer = (userDetails) => {
  const jwtToken = userDetails.token;

  socket = io("http://localhost:5002", {
    auth: {
      token: jwtToken,
    },
  });

  socket.on("connect", () => {
    console.log("succesfully connected with socket.io server");
    console.log(socket.id);
  });

  socket.on("friends-invitations", (data) => {
    const { pendingInvitations } = data;
    store.dispatch(setPendingFriendsInvitations(pendingInvitations));
  });

  socket.on("friends-list", (data) => {
    const { friends } = data;
    store.dispatch(setFriends(friends));
  });

  socket.on("online-users", (data) => {
    const { onlineUsers } = data;
    store.dispatch(setOnlineUsers(onlineUsers));
  });

  socket.on("direct-chat-history", (data) => {
    console.log(data);
    updateDirectChatHistoryIfActive(data);
  });

  socket.on("get-groups", (data) => {
    const { groups } = data;
    console.log("groups from server: ", groups);
    store.dispatch(setGroups(groups));
  });

  socket.on("direct-group-chat-history", (data) => {
    console.log("group-chat-history: ", data);
    store.dispatch(groupChatReceived(data));
  });

  socket.on("new-group-message", (data) => {
    console.log("new-group-message-arrived: ", data);
    store.dispatch(newMessageReceived(data));
  });
};

export const sendDirectMessage = (data) => {
  console.log(data);
  socket.emit("direct-message", data);
};

export const getDirectChatHistory = (data) => {
  socket.emit("direct-chat-history", data);
};

export const sendDirectGroupMessage = (data) => {
  socket.emit("direct-group-message", data);
};

export const getDirectGroupChatHistory = (data) => {
  socket.emit("direct-group-chat-history", data);
};

export const joinRoom = (data) => {
  const { groupId } = data;
  socket.emit("join-room", { groupId });
};
