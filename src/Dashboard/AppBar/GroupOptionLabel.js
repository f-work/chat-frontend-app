import React, { useState } from "react";
import { connect } from "react-redux";
import { Typography } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { styled } from "@mui/system";
import AddFriendToGroup from "./AddFriendToGroup";

const GroupOptionLabelContainer = styled("div")({
  display: "flex",
});

const GroupOptionLabel = ({ name, currentGroup }) => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const loggedInUser = JSON.parse(window.localStorage.getItem("user"));
  console.log("admin: ", currentGroup.admin);
  console.log("logged in user: ", loggedInUser);

  return (
    <GroupOptionLabelContainer>
      <Typography sx={{ fontSize: "16px", color: "white", fontWeight: "bold" }}>
        {currentGroup?.name && "Chosen Group: " + currentGroup?.name}
      </Typography>
      {currentGroup?.name && loggedInUser._id === currentGroup.admin._id && (
        <span onClick={() => setIsDialogOpen(true)}>
          <AddIcon
            style={{ color: "white", marginLeft: "20px", cursor: "pointer" }}
          />
        </span>
      )}
      <AddFriendToGroup
        group={currentGroup}
        isDialogOpen={isDialogOpen}
        closeDialogHandler={() => setIsDialogOpen(false)}
      />
    </GroupOptionLabelContainer>
  );
};

const mapStoreStateToProps = (state) => {
  return {
    name: state.chat.chosenChatDetails?.name,
    currentGroup: state.group.currentGroup,
  };
};

export default connect(mapStoreStateToProps)(GroupOptionLabel);
