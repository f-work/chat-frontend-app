import { styled } from "@mui/system";
import AddFriendButton from "../FriendsSideBar/AddFriendButton";
import AddGroupButton from "./AddGroupButton";
import GroupList from "./GroupList";

const MainContainer = styled("div")({
  width: "224px",
  height: "100%",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  backgroundColor: "#2F3136",
});

const GroupSideBar = () => {
  return (
    <MainContainer>
      <AddGroupButton />
      <GroupList />
    </MainContainer>
  );
};

export default GroupSideBar;
