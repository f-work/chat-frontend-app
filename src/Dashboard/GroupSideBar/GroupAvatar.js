import React from "react";
import { styled } from "@mui/system";

const AvatarPreview = styled("div")({
  height: "42px",
  width: "42px",
  backgroundColor: "#5865f2",
  borderRadius: "42px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  fontSize: "20px",
  fontWeight: "700",
  marginLeft: "5px",
  color: "white",
});

const imgURL =
  "https://images.unsplash.com/photo-1419242902214-272b3f66ee7a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8bmlnaHR8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60";

const GroupAvatar = ({ image, large }) => {
  return (
    <AvatarPreview style={large ? { height: "80px", width: "80px" } : {}}>
      <img
        src={image}
        alt="Group Icon"
        style={{
          width: "100%",
          height: "100%",
          overflow: "hidden",
          borderRadius: "42px",
        }}
      />
    </AvatarPreview>
  );
};

export default GroupAvatar;
