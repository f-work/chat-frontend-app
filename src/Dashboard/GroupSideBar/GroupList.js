import React from "react";
import { styled } from "@mui/system";

import { connect } from "react-redux";
import FriendsListItem from "../FriendsSideBar/FriendsList/FriendsListItem";
import GroupListItem from "./GroupListItem";

const MainContainer = styled("div")({
  flexGrow: 1,
  width: "100%",
  marginTop: "20px",
});

const GROUPS = [
  { id: 1, name: "First Group" },
  { id: 2, name: "Second Group" },
  { id: 3, name: "Third Group" },
];

const GroupList = ({ groups }) => {
  return (
    <MainContainer>
      {groups.map((grp) => (
        <GroupListItem key={grp._id} grp={grp} />
      ))}
    </MainContainer>
  );
};

const mapStoreStateToProps = ({ group }) => {
  return {
    ...group,
  };
};

export default connect(mapStoreStateToProps)(GroupList);
