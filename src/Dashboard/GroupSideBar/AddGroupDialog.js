import React, { useState, useEffect } from "react";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import Typography from "@mui/material/Typography";
import { validateMail } from "../../shared/utils/validators";
import InputWithLabel from "../../shared/components/InputWithLabel";
import CustomPrimaryButton from "../../shared/components/CustomPrimaryButton";
import { connect } from "react-redux";
import { getActions } from "../../store/actions/group";
import { styled } from "@mui/system";
import * as apis from "../../api";

const GroupIconContainer = styled("div")({
  marginTop: "20px",
  display: "flex",
  justifyContent: "center",
});

const AddGroupDialog = ({
  isDialogOpen,
  closeDialogHandler,
  setGroup = () => {},
}) => {
  const [grpName, setGrpName] = useState("");
  const [grpIconURL, setGrpIconURL] = useState("");
  const [isFormValid, setIsFormValid] = useState("");

  const handleSave = async () => {
    try {
      const res = await apis.createNewGroup({
        name: grpName,
        image: grpIconURL,
      });
      console.log("new group: ", res.data.newGroup);
      setGroup({ data: res.data.newGroup, hasError: false });
      handleCloseDialog();
    } catch (err) {
      console.log("error: ", err.response.data);
      setGroup({ data: err.response.data, hasError: true });
    }
  };

  const handleCloseDialog = () => {
    closeDialogHandler();
    setGrpName("");
    setGrpIconURL("");
  };

  useEffect(() => {
    if (grpName !== "" && grpIconURL !== "") {
      setIsFormValid(true);
    } else {
      setIsFormValid(false);
    }
  }, [grpName, grpIconURL, setIsFormValid]);

  return (
    <div>
      <Dialog open={isDialogOpen} onClose={handleCloseDialog}>
        <DialogTitle>
          <Typography>Create New Group</Typography>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <Typography>
              Create Group and add your friends you can chat with ...
            </Typography>
          </DialogContentText>
          <InputWithLabel
            label="Group Name"
            type="text"
            value={grpName}
            setValue={setGrpName}
            placeholder="Discussion Group .."
          />
          <InputWithLabel
            label="Group Icon"
            type="text"
            value={grpIconURL}
            setValue={setGrpIconURL}
            placeholder="Group Icon URL"
          />
          {grpIconURL && (
            <GroupIconContainer>
              <img width="200" height="150" src={grpIconURL} alt="Group Icon" />
            </GroupIconContainer>
          )}
        </DialogContent>
        <DialogActions>
          <CustomPrimaryButton
            onClick={handleSave}
            disabled={!isFormValid}
            label="Save"
            additionalStyles={{
              marginLeft: "15px",
              marginRight: "15px",
              marginBottom: "10px",
            }}
          />
        </DialogActions>
      </Dialog>
    </div>
  );
};

const mapActionsToProps = (dispatch) => {
  return {
    ...getActions(dispatch),
  };
};

export default connect(null, mapActionsToProps)(AddGroupDialog);
