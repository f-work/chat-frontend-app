import React from "react";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { getActions } from "../../store/actions/group";
import { connect } from "react-redux";
import GroupAvatar from "./GroupAvatar";
import { joinRoom } from "../../realtimeCommunication/socketConnection";

const GroupListItem = ({ grp, switchGroup }) => {
  const handleChooseActiveConversation = () => {
    joinRoom({ groupId: grp._id });
    switchGroup(grp);
  };

  return (
    <Button
      onClick={handleChooseActiveConversation}
      style={{
        width: "100%",
        height: "42px",
        marginTop: "10px",
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-start",
        textTransform: "none",
        color: "black",
        position: "relative",
      }}
    >
      {/* <Avatar username={username} /> */}
      <GroupAvatar image={grp.image} />
      <Typography
        style={{
          marginLeft: "7px",
          fontWeight: 700,
          color: "#8e9297",
        }}
        variant="subtitle1"
        align="left"
      >
        {grp.name}
      </Typography>
    </Button>
  );
};

const mapActionsToProps = (dispatch) => {
  return {
    ...getActions(dispatch),
  };
};

export default connect(null, mapActionsToProps)(GroupListItem);
