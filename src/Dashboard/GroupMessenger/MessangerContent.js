import React, { useEffect } from "react";
import { styled } from "@mui/system";
import Messages from "./Messages";
import GroupMessageInput from "./NewMessageInput";
import NewMessageInput from "../Messenger/NewMessageInput";
import { getDirectGroupChatHistory } from "../../realtimeCommunication/socketConnection";

const Wrapper = styled("div")({
  flexGrow: 1,
});

const MessengerContent = ({ currentGroup }) => {
  //   useEffect(() => {
  //     getDirectChatHistory({
  //       receiverUserId: chosenChatDetails.id,
  //     });
  //   }, [chosenChatDetails]);

  useEffect(() => {
    getDirectGroupChatHistory({ groupId: currentGroup._id });
  }, [currentGroup]);

  return (
    <Wrapper>
      <Messages />
      {/* <NewMessageInput /> */}
      <GroupMessageInput />
    </Wrapper>
  );
};

export default MessengerContent;
