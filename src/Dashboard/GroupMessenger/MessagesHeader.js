import React from "react";
import { styled } from "@mui/system";
import Avatar from "../../shared/components/Avatar";
import GroupAvatar from "../GroupSideBar/GroupAvatar";
import Typography from "@mui/material/Typography";

const MainContainer = styled("div")({
  width: "98%",
  display: "column",
  marginTop: "10px",
});

const MessagesHeader = ({ group }) => {
  return (
    <MainContainer>
      <GroupAvatar image={group.image} />
      <Typography
        variant="h4"
        sx={{
          fontWeight: "bold",
          color: "white",
          marginLeft: "5px",
          marginRight: "5px",
        }}
      >
        {group.name}
      </Typography>
      <Typography
        sx={{
          color: "#e6ecf5",
          marginLeft: "5px",
          marginRight: "5px",
        }}
      >
        This is the beginning of your conversation with {group.name}
      </Typography>
    </MainContainer>
  );
};

export default MessagesHeader;
