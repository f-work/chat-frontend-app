import React from "react";
import { styled } from "@mui/system";
import { connect } from "react-redux";
import WelcomeMessage from "../Messenger/WelcomeMessage";
import MessengerContent from "./MessangerContent";

const MainContainer = styled("div")({
  flexGrow: 1,
  backgroundColor: "#36393f",
  marginTop: "48px",
  display: "flex",
});

const GroupMessenger = ({ currentGroup }) => {
  return (
    <MainContainer>
      {!currentGroup?.name ? (
        <WelcomeMessage />
      ) : (
        <MessengerContent currentGroup={currentGroup} />
      )}
    </MainContainer>
  );
};

const mapStoreStateToProps = (state) => {
  return {
    currentGroup: state.group.currentGroup,
  };
};

export default connect(mapStoreStateToProps)(GroupMessenger);
