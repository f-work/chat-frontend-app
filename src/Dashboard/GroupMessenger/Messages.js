import React, { userRef, useEffect } from "react";
import { styled } from "@mui/system";
import MessagesHeader from "./MessagesHeader";
import { connect } from "react-redux";
// import DUMMY_MESSAGES from "./DUMMY_MESSAGES";
import Message from "../Messenger/Messages/Message";
import DateSeparator from "../Messenger/Messages/DateSeparator";

const MSGS = [
  {
    _id: "123asd",
    author: {
      _id: "ffasd2",
      name: "Smith",
    },
    content: "hello",
    type: "DIRECT",
    date: "2021-05-03",
  },
  {
    _id: "123asd1",
    author: {
      _id: "ffasd2",
      name: "Smith",
    },
    content: "hello 1",
    type: "DIRECT",
    date: "2021-05-03",
  },
  {
    _id: "123asd2",
    author: {
      _id: "ffasd2",
      name: "Smith",
    },
    content: "hello 2",
    type: "DIRECT",
    date: "2021-05-03",
  },
  {
    _id: "123asd3",
    author: {
      _id: "ffasd2",
      name: "Smith",
    },
    content: "hello 3",
    type: "DIRECT",
    date: "2021-05-03",
  },
];

const MainContainer = styled("div")({
  height: "calc(100% - 60px)",
  overflow: "auto",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
});

const convertDateToHumanReadable = (date, format) => {
  const map = {
    mm: date.getMonth() + 1,
    dd: date.getDate(),
    yy: date.getFullYear().toString().slice(-2),
    yyyy: date.getFullYear(),
  };

  return format.replace(/mm|dd|yy|yyy/gi, (matched) => map[matched]);
};

const Messages = ({ chosenChatDetails, chatHistory, currentGroup }) => {
  console.log("chat--history: ", chatHistory);

  return (
    <MainContainer>
      <MessagesHeader group={currentGroup} />
      {chatHistory.map((chat, index) => {
        const sameAuthor =
          index > 0 &&
          chatHistory[index].message.author._id ===
            chatHistory[index - 1].message.author._id;

        const sameDay =
          index > 0 &&
          convertDateToHumanReadable(
            new Date(chat.message.date),
            "dd/mm/yy"
          ) ===
            convertDateToHumanReadable(
              new Date(chatHistory[index - 1].message.date),
              "dd/mm/yy"
            );
        // const sameDay = "";
        // const sameAuthor = "";

        return (
          <div key={chat.message._id} style={{ width: "97%" }}>
            {(!sameDay || index === 0) && (
              <DateSeparator
                date={convertDateToHumanReadable(
                  new Date(chat.message.date),
                  "dd/mm/yy"
                )}
              />
            )}
            <Message
              content={chat.message.content}
              username={chat.message.author.username}
              sameAuthor={sameAuthor}
              date={convertDateToHumanReadable(
                new Date(chat.message.date),
                "dd/mm/yy"
              )}
              sameDay={sameDay}
              userId={chat.message.author._id}
            />
          </div>
        );
      })}
    </MainContainer>
  );
};

const mapStoreStateToProps = ({ groupChat, group }) => {
  return {
    ...groupChat,
    currentGroup: group.currentGroup,
  };
};

export default connect(mapStoreStateToProps)(Messages);
