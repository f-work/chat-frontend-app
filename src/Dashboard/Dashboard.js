import React, { useEffect } from "react";
import { styled } from "@mui/system";
import SideBar from "./SideBar/SideBar";
import FriendsSideBar from "./FriendsSideBar/FriendsSideBar";
import Messenger from "./Messenger/Messenger";
import AppBar from "./AppBar/AppBar";
import { logout } from "../shared/utils/auth";
import { connect } from "react-redux";
import { getActions } from "../store/actions/authActions";
import { connectWithSocketServer } from "../realtimeCommunication/socketConnection";
import GroupSideBar from "./GroupSideBar";
import GroupMessenger from "./GroupMessenger";

const Wrapper = styled("div")({
  width: "100%",
  height: "100vh",
  display: "flex",
});

const TABS = { user: "user", group: "group" };

const Dashboard = ({ setUserDetails, currentTab }) => {
  useEffect(() => {
    const userDetails = localStorage.getItem("user");

    if (!userDetails) {
      logout();
    } else {
      setUserDetails(JSON.parse(userDetails));
      connectWithSocketServer(JSON.parse(userDetails));
    }
  }, []);

  if (currentTab === TABS.user) {
    return (
      <Wrapper>
        <SideBar />
        <FriendsSideBar />
        <Messenger />
        <AppBar currentTab={currentTab} />
      </Wrapper>
    );
  }

  return (
    <Wrapper>
      <SideBar />
      <GroupSideBar />
      <GroupMessenger />
      <AppBar currentTab={currentTab} />
    </Wrapper>
  );
};

const mapActionsToProps = (dispatch) => {
  return {
    ...getActions(dispatch),
  };
};

const mapStoreStateToProps = ({ dashboard }) => {
  return { ...dashboard };
};

export default connect(mapStoreStateToProps, mapActionsToProps)(Dashboard);
