import React from "react";
import Button from "@mui/material/Button";
import GroupsIcon from "@mui/icons-material/Groups";
import UserIcon from "@mui/icons-material/SupervisedUserCircle";
import { styled } from "@mui/system";
import { connect } from "react-redux";
import { getActions } from "../../store/actions/dashboard";

const LinkToGroupPage = styled("p")({
  color: "white",
  cursor: "pointer",
});

const MainPageButton = ({ changeAction }) => {
  return (
    <>
      <Button
        style={{
          width: "48px",
          height: "48px",
          borderRadius: "16px",
          margin: 0,
          padding: 0,
          minWidth: 0,
          marginTop: "10px",
          color: "white",
          backgroundColor: "#5865F2",
        }}
        onClick={() => changeAction("user")}
      >
        <UserIcon />
      </Button>
      <Button
        style={{
          width: "48px",
          height: "48px",
          borderRadius: "16px",
          margin: 0,
          padding: 0,
          minWidth: 0,
          marginTop: "10px",
          color: "white",
          backgroundColor: "#5865F2",
        }}
        onClick={() => changeAction("group")}
      >
        <GroupsIcon />
      </Button>
    </>
  );
};

const mapActionsToProps = (dispatch) => {
  return {
    ...getActions(dispatch),
  };
};

export default connect(null, mapActionsToProps)(MainPageButton);
