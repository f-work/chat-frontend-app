import React from "react";
import { styled } from "@mui/system";
import Avatar from "../../../shared/components/Avatar";
import Typography from "@mui/material/Typography";

const MainContainer = styled("div")({
  width: "97%",
  display: "flex",
  marginTop: "10px",
  justifyContent: "flex-start",
});

const AvatarContainer = styled("div")({
  width: "70px",
});

const MessageContainer = styled("div")({
  display: "flex",
  flexDirection: "column",
});

const MessageContent = styled("div")({
  padding: "5px",
  color: "#ebf4fc",
  borderRadius: "10px",
  backgroundColor: "#5865f2",
});

const SameAuthorMessageContent = styled("div")({
  color: "#DCDDDE",
  width: "97%",
  display: "flex",
});

const SameAuthorMessageText = styled("span")({
  marginLeft: "70px",
});

const Message = ({ content, sameAuthor, username, date, sameDay, userId }) => {
  const loggedInUser = JSON.parse(window.localStorage.getItem("user"));

  const isMessageOnRight = loggedInUser._id === userId;

  if (sameAuthor && sameDay) {
    return (
      <SameAuthorMessageContent style={{ justifyContent: isMessageOnRight ? "flex-end": "flex-start"}}>
        <SameAuthorMessageText>{content}</SameAuthorMessageText>
      </SameAuthorMessageContent>
    );
  }

  return (
    <MainContainer style={{ justifyContent: isMessageOnRight ? "flex-end": "flex-start"}}>
      <AvatarContainer>
        <Avatar username={username} />
      </AvatarContainer>
      <MessageContainer>
        <Typography style={{ fontSize: "16px", color: "white" }}>
          {username}{" "}
          <span style={{ fontSize: "12px", color: "#72767d" }}>{date}</span>
        </Typography>
        <MessageContent>{content}</MessageContent>
      </MessageContainer>
    </MainContainer>
  );
};

export default Message;
