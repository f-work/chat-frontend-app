import dashboardActions from "../actions/dashboard";

const initState = {
  currentTab: "user",
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case dashboardActions.CHANGE_ACTION:
      return {
        ...initState,
        currentTab: action.content,
      };
    default:
      return state;
  }
};

export default reducer;
