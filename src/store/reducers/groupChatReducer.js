import groupChatActions from "../actions/groupChatAction";

const initState = {
  chatHistory: [],
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case groupChatActions.GROUP_CHAT_RECEIVED:
      return {
        ...state,
        chatHistory: action.content,
      };
    case groupChatActions.NEW_MESSAGE_RECEIVED:
      return {
        ...state,
        chatHistory: [...state.chatHistory, action.content],
      };
    default:
      return state;
  }
};

export default reducer;
