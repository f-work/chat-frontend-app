import groupActions from "../actions/group";

const initState = {
  currentGroup: {},
  groups: [],
  isLoading: false,
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case groupActions.GROUPS_REQUESTED:
      return {
        ...state,
        isLoading: true,
      };
    case groupActions.GROUPS_RECEIVED:
      return {
        ...state,
        isLoading: false,
        groups: action.content,
      };
    case groupActions.SWITCH_GROUP:
      return {
        ...state,
        currentGroup: action.content,
      };
    case groupActions.NEW_GROUP_ADDED:
      return {
        ...state,
        currentGroup: action.content,
        groups: [...state.groups, action.content],
      };
    default:
      return state;
  }
};

export default reducer;
