import { openAlertMessage } from "./alertActions";

const dashboardActions = {
  GROUPS_REQUESTED: "GROUPS.GROUPS_REQUESTED",
  GROUPS_RECEIVED: "GROUPS.GROUPS_RECEIVED",
  SWITCH_GROUP: "GROUPS.SWITCH_GROUP",
  NEW_GROUP_ADDED: "GROUPS.NEW_GROUP_ADDED",
};

export const getActions = (dispatch) => {
  return {
    groupsRequested: (content) => dispatch(groupsRequested(content)),
    groupsReceived: (content) => dispatch(groupsReceived(content)),
    switchGroup: (content) => dispatch(switchGroup(content)),
    setGroup: (content) => dispatch(setGroup(content)),
    addUserToGroup: (content) => dispatch(addUserToGroup(content)),
  };
};

export const switchGroup = (content) => {
  return {
    type: dashboardActions.SWITCH_GROUP,
    content,
  };
};

export const groupsRequested = (content) => {
  return {
    type: dashboardActions.GROUPS_REQUESTED,
    content,
  };
};

export const groupsReceived = (content) => {
  return {
    type: dashboardActions.GROUPS_RECEIVED,
    content,
  };
};

export const newGroupAdded = (content) => {
  return {
    type: dashboardActions.NEW_GROUP_ADDED,
    content,
  };
};

export default dashboardActions;

export const setGroups = (groups) => {
  return (dispatch) => {
    dispatch(groupsReceived(groups));
  };
};

export const setGroup = (obj) => {
  return (dispatch) => {
    const { data, hasError } = obj;
    console.log("data: ", data);
    console.log("hasError: ", hasError);

    if (!hasError) {
      dispatch(newGroupAdded(data));
      dispatch(openAlertMessage("Created New Group"));
    } else {
      dispatch(openAlertMessage(data));
    }
  };
};

export const addUserToGroup = (obj) => {
  return (dispatch) => {
    const { data, hasError } = obj;

    if (!hasError) {
      dispatch(openAlertMessage(data));
    } else {
      dispatch(openAlertMessage(data));
    }
  };
};
