const groupChatActions = {
  GROUP_CHAT_RECEIVED: "GROUP_CHAT:GROUP_CHAT_RECEIVED",
  NEW_MESSAGE_RECEIVED: "GROUP_CHAT:NEW_MESSAGE_RECEIVED",
};

export const getActions = (dispatch) => {
  return {
    groupChatReceived: (content) => dispatch(groupChatReceived(content)),
    newMessageReceived: (content) => dispatch(newMessageReceived(content)),
  };
};

export const groupChatReceived = (content) => {
  return {
    type: groupChatActions.GROUP_CHAT_RECEIVED,
    content,
  };
};

export const newMessageReceived = (content) => {
  return {
    type: groupChatActions.NEW_MESSAGE_RECEIVED,
    content,
  };
};

export default groupChatActions;
