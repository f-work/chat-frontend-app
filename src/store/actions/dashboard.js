const dashboardActions = {
  CHANGE_ACTION: "DASHBOARD.CHANGE_ACTION",
};

export const getActions = (dispatch) => {
  return {
    changeAction: (content) => dispatch(changeAction(content)),
  };
};

export const changeAction = (content) => {
  return {
    type: dashboardActions.CHANGE_ACTION,
    content,
  };
};

export default dashboardActions;
